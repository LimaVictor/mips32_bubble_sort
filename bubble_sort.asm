.data                  	# directive indicating start of the data segment
.align    2            	# set data alignment to 4 bytes

array:                 	
.word    7, 2, 3, 5, 9, 8, 1, 0, -10    # values in the array

.text                	# beginning of the text segment (or code segment)

start:
addi $s4, $zero, 1	# change=1

while:
  beq  $s4, $zero, done 	# while (change=1)
  addi $s1, $zero, 0   		# initialization instruction of for cycle: i=0, kde i=s1
  la   $s0, array 		# store address of the "array" to the register s0
  addi $s4, $zero, 0		#change = 0
  
inner_for:
  beq  $s1, 8, while		# 8 - quantities of number in "array"
  lw   $s2, 0x0($s0)    	# load value from the array to s2
  addi $s0, $s0, 0x4    	# increment offset and move to the other value in the array
  lw   $s3, 0x0($s0)    	# load value from the array to s3
  slt  $s5, $s2, $s3		# if s2 less then s3 -- set s5 to 1
  addi $s1, $s1, 1		#inc i cykl
  beq  $s5, 1, inner_for	# if (s2 < s3) skip, else continue
  
  sw   $s2, 0x0($s0)
  addi $s0, $s0, -0x4		#change offset
  sw   $s3, 0x0($s0)
  addi $s0, $s0, 0x4 
  addi $s4, $zero, 1
  
  j inner_for
  
  
done:
nop
